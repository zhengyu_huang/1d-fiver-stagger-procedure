__author__ = 'daniel'



from Time_integrator import *

# L is the length of the tube, assuming that the tube is at [0, L]
# N is the number of fluid ceils
# structure_inf = [xs_0, vs_0, ks, ms], which are parameters for structure
# fluid_inf =[gamma,rho_l, v_l, p_l], which are parameters for the fluid
# mesh  = [L, N ,CFL]








def FSI_1D(mesh, fluid_info, structure_info, time_info, problem_type):

    [L,N,CFL] = mesh;

    [gamma, rho_l, v_l, p_l] = fluid_info

    [ms,ks, thickness] = structure_info

    [time, time_type] = time_info

    [method, implicit_or_explicit, forced_or_aero] = problem_type;

    eos = [gamma]

    fluid = Fluid(N,L)

    fluid.Const_initial(fluid_info)

    structure = Structure(ms, ks, N, L, thickness)

    dt = fluid.dx*CFL

    max_wave_speed = 0

    t = 0


    if(method == 'A6'):
        print('start A6 method')

        A6_First_Step(fluid,structure,time, dt/2, eos,problem_type)

        t = t + dt/2

        while t < time - 1e-8:



            Structure_Time_Advance(fluid, structure, time, dt, eos,problem_type)

            Fluid_Time_Advance(fluid, structure, time, dt, eos,problem_type)

            max_wave_speed = max(Max_Wave_Speed(structure,fluid,eos), max_wave_speed)

            t = t + dt


    elif(method == 'A7'):
        structure.Move_forward(p_l,dt,problem_type)

        while t < time - 1e-8:


            Fluid_Time_Advance(fluid, structure, time, dt, eos,problem_type)

            Structure_Time_Advance(fluid, structure, time, dt, eos,problem_type)

            max_wave_speed = max(Max_Wave_Speed(structure,fluid,eos), max_wave_speed)


            t = t + dt;



    print("Finish at time ", t, " structure time is ", structure.t, " fluid time is ", fluid.t)

    print('Max wave speed is ', max_wave_speed)

    return fluid, structure

