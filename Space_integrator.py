__author__ = 'daniel'


from FVM_utility import *


def Compute_Rhs_Muscl(ww, fluid, structure ,  step,eos):

    eps = 10**(-10)

    N_nodes = fluid.n;

    flux = np.zeros([3, N_nodes + 1])

    rhs  = np.zeros([3, N_nodes])

    dx = fluid.dx   #assume all the cells have the same size

    xx = fluid.xx

    Interpolation_check = np.array([dx, INTERPOLATION_TOL])

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    if( step == 1):
        FS_id = FS_id_old_n

        [xs,vs,as_] = structure.qq_old_n
    else:


        FS_id = FS_id_n

        [xs,vs,as_] = structure.qq_n


        #GTR in the second step of RK
        if(FS_id_n == FS_id_old_n + 1):

            x_si = 0.5*(xx[FS_id_old_n] + xx[FS_id_old_n + 1])

            #[xs, vs, as_] = structure.qq_n

            fluid_normal = 1.0

            ########### Could be optimized UNDO
            ##################
            ww_s = SIV(xx[FS_id_old_n - 2], ww[:, FS_id_old_n - 2], xx[FS_id_old_n - 1], ww[:,FS_id_old_n - 1], x_si, xs, vs, eos, SIV_ORDER, fluid_normal, Interpolation_check)

            ww[:,FS_id_n - 1] = Interpolation(ww[:,FS_id_old_n - 1],xx[FS_id_old_n - 1], ww_s, x_si, xx[FS_id_old_n], eos,  Interpolation_check)










    #deal with wall boundary
    #solving one-sided Rienmann problem on the wall boundary
    #to impose the boundary condition v_wall = 0
    #and compute the physical flux on the wall boundary


    w_wall = SIV(xx[0],ww[:,0],xx[1],ww[:,1],0.0, 0.0, 0.0, eos, 2 , -1.0, Interpolation_check)

    flux[:,0] = Flux(w_wall,eos)


    #Ghost Fluid Method with Roe Flux to implement wall boundary condition
    #ww_l = np.array([ww[0,0],-ww[1,0],ww[2,0]])
    #flux[:, 0] = FF_Roe_Flux(ww_l, ww[:,0], eos)


    #FS_id is the number of the edge which intersects the structure surface
    for i in range(1, int(FS_id) + 1):
        #################################################
        # Left State
        #######################################################

        if(i == 0):
            ww_ll = 3.0*ww[:,0] - 2.0*ww[:,1]
            ww_l  = 2.0*ww[:,0] -     ww[:,1]

            xx_ll = xx[0] - 2.0*dx
            xx_l  = xx[0] -     dx

        elif(i == 1):

            xx_ll = xx[0] - dx
            xx_l  = xx[0]

            ww_l  = ww[:,0]


            ww_ll = Interpolation(w_wall,0.0,ww_l,xx_l,xx_ll,eos, Interpolation_check)

            #Ghost Fluid Method exterpolation
            #ww_ll = np.array([ww[0,0],-ww[1,0],ww[2,0]])
        else:

            xx_ll = xx[i - 2]
            xx_l  = xx[i - 1]

            ww_ll = ww[:,i - 2]
            ww_l  = ww[:,i - 1]


        ########################################
        # Right State
        ##############################################################
        if(i == N_nodes):
            ww_rr = 3.0*ww[:,N_nodes - 1] - 2.0*ww[:,N_nodes - 2]
            ww_r  = 2.0*ww[:,N_nodes - 1] -     ww[:,N_nodes - 2]

            xx_rr = xx[N_nodes - 1] + 2.0*dx
            xx_r  = xx[N_nodes - 1] + 1.0*dx
        elif(i == N_nodes - 1):
            ww_rr = 2.0*ww[:,i] - ww[:,i - 1]
            ww_r  = ww[:,i]

            xx_rr = xx[i] + dx
            xx_r  = xx[i]
        else:
            ww_r  = ww[:,i]
            ww_rr = ww[:,i + 1]

            xx_r  = xx[i]
            xx_rr = xx[i + 1]

        ######################
        # Left State
        #######################################

        ww_L_HO = 0.0
        ww_R_HO = 0.0
        if( i < FS_id - 1 ):
            L = (ww_r - ww_l)/(ww_l - ww_ll + eps);

            phi = Limiter_Fun(L)

            ww_L_HO = ww_l + 0.5* phi *(ww_l - ww_ll)



            R = (ww_r - ww_l)/(ww_rr - ww_r + eps);

            phi = Limiter_Fun(R)

            ww_R_HO = ww_r - 0.5* phi *(ww_rr - ww_r)

            flux[:, i] = FF_Roe_Flux(ww_L_HO, ww_R_HO, eos)

        elif(i == FS_id - 1 ):

            L = (ww_r - ww_l)/(ww_l - ww_ll + eps);

            phi = Limiter_Fun(L)

            ww_L_HO = ww_l + 0.5* phi *(ww_l - ww_ll)


            x_si = 0.5*(xx_r + xx_rr)
#?????????????????????????



            ww_s = SIV(xx_l, ww_l, xx_r, ww_r, x_si, xs, vs, eos, SIV_ORDER, 1.0,  Interpolation_check)

            ww_rr_g = Interpolation(ww_r, xx_r, ww_s, x_si, xx_rr,eos, Interpolation_check)

            R = (ww_r - ww_l)/(ww_rr_g - ww_r + eps);

            phi = Limiter_Fun(R)

            ww_R_HO = ww_r - 0.5* phi *(ww_rr_g - ww_r)

            flux[:, i] = FF_Roe_Flux(ww_L_HO, ww_R_HO, eos)

        else:

            x_si = 0.5*(xx_l + xx_r)


            ww_s = SIV(xx_ll, ww_ll, xx_l, ww_l, x_si, xs, vs, eos, SIV_ORDER, 1.0,  Interpolation_check)


            flux[:,i] = Flux(ww_s, eos)

    for i in range(int(FS_id)):

        rhs[:,i] = (flux[:, i + 1] - flux[:, i])/dx

    return rhs


#The jacobian matrix is like
# A_0 C_1
# B_1 A_1 C_2
#     B_2 A_2 C_3
#             .
#                 .
#                      .
#                     B_n-1 A_n-1


def Compute_Jacobian(ww, fluid, structure,dt):

    eps = 10**(-10)

    eos = fluid.eos

    N_nodes = fluid.n;

    flux = np.zeros([3, N_nodes + 1])

    rhs  = np.zeros([3, N_nodes])

    dx = fluid.dx   #assume all the cells have the same size

    xx = fluid.xx

    Interpolation_check = np.array([dx, INTERPOLATION_TOL])

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )


    FS_id = FS_id_old_n

    [xs,vs,as_] = structure.qq_n

    A = np.zeros([FS_id,3,3])
    B = np.zeros([FS_id,3,3])
    C = np.zeros([FS_id,3,3])


    #FS_id is the number of the edge which intersects the structure surface
    for i in range(int(FS_id) + 1):
        #################################################
        # Left State
        #######################################################

        if(i == 0):
            ww_ll = np.array([ww[0,1],-ww[1,1],ww[2,1]])
            ww_l  = np.array([ww[0,0],-ww[1,0],ww[2,0]])

            xx_ll = xx[0] - 2.0*dx
            xx_l  = xx[0] -     dx

        elif(i == 1):

            xx_ll = xx[0] - dx
            xx_l  = xx[0]

            ww_l  = ww[:,0]


            ww_ll = np.array([ww[0,0],-ww[1,0],ww[2,0]])
        else:

            xx_ll = xx[i - 2]
            xx_l  = xx[i - 1]

            ww_ll = ww[:,i - 2]
            ww_l  = ww[:,i - 1]


        ########################################
        # Right State
        ##############################################################
        if(i == N_nodes):
            ww_rr = 3.0*ww[:,N_nodes - 1] - 2.0*ww[:,N_nodes - 2]
            ww_r  = 2.0*ww[:,N_nodes - 1] -     ww[:,N_nodes - 2]

            xx_rr = xx[N_nodes - 1] + 2.0*dx
            xx_r  = xx[N_nodes - 1] + 1.0*dx
        elif(i == N_nodes - 1):
            ww_rr = 2.0*ww[:,i] - ww[:,i - 1]
            ww_r  = ww[:,i]

            xx_rr = xx[i] + dx
            xx_r  = xx[i]
        else:
            ww_r  = ww[:,i]
            ww_rr = ww[:,i + 1]

            xx_r  = xx[i]
            xx_rr = xx[i + 1]

        ######################
        # Left State
        #######################################

        ww_L_HO = 0.0
        ww_R_HO = 0.0
        if(i == 0):
            L = (ww_r - ww_l)/(ww_l - ww_ll + eps);

            phi = Limiter_Fun(L)

            ww_L_HO = ww_l + 0.5* phi *(ww_l - ww_ll)

            dww_L_HO_dww_r = np.diag([1 + 0.5*phi[0],-1 - 0.5*phi[1],1 + 0.5*phi[2] ])


            R = (ww_r - ww_l)/(ww_rr - ww_r + eps);

            phi = Limiter_Fun(R)

            ww_R_HO = ww_r - 0.5* phi *(ww_rr - ww_r)

            dww_R_HO_dww_r = np.diag(1 + 0.5*phi)

            jac_l,jac_r = FF_Roe_Jacobian(ww_L_HO, ww_R_HO, eos)

            A[0] = A[0] - dt/dx*(jac_l*dww_L_HO_dww_r + jac_r*dww_R_HO_dww_r)



        elif( i < FS_id - 1 ):
            L = (ww_r - ww_l)/(ww_l - ww_ll + eps);

            phi = Limiter_Fun(L)

            ww_L_HO = ww_l + 0.5* phi *(ww_l - ww_ll)

            dww_L_HO_dww_l = np.diag(1 + 0.5*phi)

            R = (ww_r - ww_l)/(ww_rr - ww_r + eps);

            phi = Limiter_Fun(R)

            ww_R_HO = ww_r - 0.5* phi *(ww_rr - ww_r)

            dww_R_HO_dww_r = np.diag(1 + 0.5*phi)

            jac_l,jac_r = FF_Roe_Jacobian(ww_L_HO, ww_R_HO, eos)



            A[i-1] = A[i-1] + dt/dx*jac_l*dww_L_HO_dww_l



            A[i] = A[i] - dt/dx*jac_r*dww_R_HO_dww_r

            C[i] = C[i] + dt/dx*jac_r*dww_R_HO_dww_r

            B[i] = B[i] - dt/dx*jac_l*dww_L_HO_dww_l




        elif(i == FS_id - 1 ):

            L = (ww_r - ww_l)/(ww_l - ww_ll + eps);

            phi = Limiter_Fun(L)

            ww_L_HO = ww_l + 0.5* phi *(ww_l - ww_ll)

            dww_L_HO_dww_l = np.diag(1 + 0.5*phi)


            x_si = 0.5*(xx_r + xx_rr)
#?????????????????????????



            ww_s = SIV(xx_l, ww_l, xx_r, ww_r, x_si, xs, vs, eos, SIV_ORDER, 1.0,  Interpolation_check)

            ww_rr_g = Interpolation(ww_r, xx_r, ww_s, x_si, xx_rr,eos, Interpolation_check)

            R = (ww_r - ww_l)/(ww_rr_g - ww_r + eps);

            phi = Limiter_Fun(R)

            ww_R_HO = ww_r - 0.5* phi *(ww_rr_g - ww_r)

            dww_R_HO_dww_r = np.diag(1 + 0.5*phi)

            jac_l,jac_r = FF_Roe_Jacobian(ww_L_HO, ww_R_HO, eos)

            A[i-1] = A[i-1] + dt/dx*jac_l*dww_L_HO_dww_l

            A[i] = A[i] - dt/dx*jac_r*dww_R_HO_dww_r

            C[i] = C[i] + dt/dx*jac_r*dww_R_HO_dww_r

            B[i] = B[i] - dt/dx*jac_l*dww_L_HO_dww_l

        else:

            x_si = 0.5*(xx_l + xx_r)


            ww_s = SIV(xx_ll, ww_ll, xx_l, ww_l, x_si, xs, vs, eos, SIV_ORDER, 1.0,  Interpolation_check)


            flux[:,i] = Flux(ww_s, eos)


            dww_L_HO_dww_l = (1 + (xs - xx_l)/(xx_l - xx_ll)) * np.diag([1.,1.,1.0])


            ww_L_HO = Interpolation(ww_ll, xx_ll, ww_l, xx_l, xs,eos, Interpolation_check)


            jac_l = FSRiemann_Jacobian(ww_L_HO, vs, eos)


            A[i-1] = A[i-1] + dt/dx*jac_l*dww_L_HO_dww_l


            B[i-1] = B[i-1] - dt/dx*jac_l*dww_L_HO_dww_l





    for i in range(int(FS_id)):

        A[i] = A[i] + 1.5*np.array([[1.,0.,0.],[0.,1.,0.],[0.,0.,1.]])


    return A,B,C


def Compute_Rhs_Muscl_Implicit(ww, fluid,structure,dt):

    eps = 10**(-10)

    eos = fluid.eos

    N_nodes = fluid.n;

    flux = np.zeros([3, N_nodes + 1])

    rhs  = np.zeros([3, N_nodes])

    dx = fluid.dx   #assume all the cells have the same size

    xx = fluid.xx

    Interpolation_check = np.array([dx, INTERPOLATION_TOL])

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )


    FS_id = FS_id_old_n

    [xs,vs,as_] = structure.qq_n




    #FS_id is the number of the edge which intersects the structure surface
    for i in range(int(FS_id) + 1):

        #################################################
        # Left State
        #######################################################

        if(i == 0):
            ww_ll = np.array([ww[0,1],-ww[1,1],ww[2,1]])
            ww_l  = np.array([ww[0,0],-ww[1,0],ww[2,0]])

            xx_ll = xx[0] - 2.0*dx
            xx_l  = xx[0] -     dx

        elif(i == 1):

            xx_ll = xx[0] - dx
            xx_l  = xx[0]

            ww_l  = ww[:,0]


            ww_ll = np.array([ww[0,0],-ww[1,0],ww[2,0]])
        else:

            xx_ll = xx[i - 2]
            xx_l  = xx[i - 1]

            ww_ll = ww[:,i - 2]
            ww_l  = ww[:,i - 1]


        ########################################
        # Right State
        ##############################################################
        if(i == N_nodes):
            ww_rr = 3.0*ww[:,N_nodes - 1] - 2.0*ww[:,N_nodes - 2]
            ww_r  = 2.0*ww[:,N_nodes - 1] -     ww[:,N_nodes - 2]

            xx_rr = xx[N_nodes - 1] + 2.0*dx
            xx_r  = xx[N_nodes - 1] + 1.0*dx
        elif(i == N_nodes - 1):
            ww_rr = 2.0*ww[:,i] - ww[:,i - 1]
            ww_r  = ww[:,i]

            xx_rr = xx[i] + dx
            xx_r  = xx[i]
        else:
            ww_r  = ww[:,i]
            ww_rr = ww[:,i + 1]

            xx_r  = xx[i]
            xx_rr = xx[i + 1]

        ######################
        # Left State
        #######################################

        ww_L_HO = 0.0
        ww_R_HO = 0.0
        if( i < FS_id - 1 ):
            L = (ww_r - ww_l)/(ww_l - ww_ll + eps);

            phi = Limiter_Fun(L)

            ww_L_HO = ww_l + 0.5* phi *(ww_l - ww_ll)



            R = (ww_r - ww_l)/(ww_rr - ww_r + eps);

            phi = Limiter_Fun(R)

            ww_R_HO = ww_r - 0.5* phi *(ww_rr - ww_r)

            flux[:, i] = FF_Roe_Flux(ww_L_HO, ww_R_HO, eos)

        elif(i == FS_id - 1 ):

            L = (ww_r - ww_l)/(ww_l - ww_ll + eps);

            phi = Limiter_Fun(L)

            ww_L_HO = ww_l + 0.5* phi *(ww_l - ww_ll)

            x_si = 0.5*(xx_r + xx_rr)

            ww_s = SIV(xx_l, ww_l, xx_r, ww_r, x_si, xs, vs, eos, SIV_ORDER, 1.0,  Interpolation_check)

            ww_rr_g = Interpolation(ww_r, xx_r, ww_s, x_si, xx_rr,eos, Interpolation_check)

            R = (ww_r - ww_l)/(ww_rr_g - ww_r + eps);

            phi = Limiter_Fun(R)

            ww_R_HO = ww_r - 0.5* phi *(ww_rr_g - ww_r)

            flux[:, i] = FF_Roe_Flux(ww_L_HO, ww_R_HO, eos)

        else:

            x_si = 0.5*(xx_l + xx_r)


            ww_s = SIV(xx_ll, ww_ll, xx_l, ww_l, x_si, xs, vs, eos, SIV_ORDER, 1.0,  Interpolation_check)


            flux[:,i] = Flux(ww_s, eos)

    for i in range(int(FS_id)):

        rhs[:,i] = (flux[:, i+1] - flux[:, i])/dx

    rhs = 1.5*ww - 2*fluid.ww_n + 0.5*fluid.ww_old_n + dt*rhs;

    return rhs
