import numpy as np
import matplotlib.pyplot as plt
L = 2.0
N = 200
ite = 5
problem_type = "aero"

if problem_type == "forced":
    mass_error = np.array([7.10878017e-05, 1.82076819e-05, 4.59396001e-06, 1.15578174e-06, 2.90240978e-07])
    energy_error = np.array([0.03343485, 0.033382, 0.03336837,  0.03336494, 0.03336408])
    title = "ForcedMotionError1D.pdf"
elif problem_type == "aero":
    mass_error = np.array([5.28955771e-06, 1.70240338e-06, 3.91348234e-07, 1.11716012e-07, 1.87493197e-08])
    energy_error = np.array([6.21216987e-06, 1.96148249e-06, 4.53153230e-07, 1.28006277e-07, 2.21523944e-08])
    title = "FSIError1D.pdf" 

axis_dx = L/N * np.array([2**(-i) for i in range(ite)])


plt.figure(figsize=(5,4))
plt.loglog(axis_dx, mass_error,'bo-' ,label = "Mass rel. error")
A = np.vstack([np.log(axis_dx), np.ones(len(axis_dx))]).T
m,c = np.linalg.lstsq(A, np.log(mass_error))[0]
t = plt.text(axis_dx[-1]*1.3, mass_error[-1], str(m)[0:4])
t.set_bbox(dict(facecolor='b', alpha=0.3, edgecolor=None))

if problem_type != "forced":
    plt.loglog(axis_dx, energy_error,'g*-', label = "Energy rel. error")
    m,c = np.linalg.lstsq(A, np.log(energy_error))[0]
    t = plt.text(axis_dx[-1], energy_error[-1]*4.0, str(m)[0:4])
    t.set_bbox(dict(facecolor='g', alpha=0.3, edgecolor=None))

plt.loglog(axis_dx, axis_dx**2,'r--',  label = "Ref. 2nd order error")

plt.grid(True)
plt.xlabel('h')
plt.ylabel('Relative errors')
plt.legend(loc='upper left')
plt.grid("on")
plt.xlim([axis_dx[ite - 1]/3.0, axis_dx[0]*3.0])
plt.tight_layout()
plt.savefig(title)
plt.close()

