__author__ = 'daniel'

import matplotlib.pyplot as plt
from FIVER import *
from Exact_solution import *


def Receding_Result(fluid_info,structure_info, time_info, problem_type, mesh):
    [L,N,CFL] = mesh
    dx = float(L)/ float(N)

    [fluid, structure] = FSI_1D(mesh, fluid_info, structure_info, time_info,problem_type)
    print("Final position of the structure is ", Structure(time_info[0]))

    [xx, exact_ww] = Exact_Solution(time_info[0], mesh, fluid_info)

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    FS_id = FS_id_n
    numerical_ww = fluid.ww_n
    numerical_uu = np.zeros([3,N])
    for i in range(0,int(FS_id)):
        numerical_uu[0,i] = numerical_ww[0,i]
        numerical_uu[1,i] = numerical_ww[1,i]/numerical_uu[0,i]
        numerical_uu[2,i] = numerical_ww[2,i]/numerical_uu[0,i]

    exact_uu = np.zeros([3,N])
    for i in range(0,int(FS_id)):
        exact_uu[0,i] = exact_ww[0,i]
        exact_uu[1,i] = exact_ww[1,i]/exact_uu[0,i]
        exact_uu[2,i] = exact_ww[2,i]/exact_uu[0,i]



    plt.figure(1)
    plt.plot(xx, exact_uu[0,:],'ro')
    plt.plot(xx, numerical_uu[0,:],'b-',label = "density")
    plt.legend(loc='upper right')
    plt.title('receding forced motion L = %d'%(L))
    plt.figure(2)
    plt.plot(xx, exact_uu[1,:],'go')
    plt.plot(xx, numerical_uu[1,:],'b-',label = "velocity")
    plt.legend(loc='upper right')
    plt.title('receding forced motion L = %d'%(L))
    plt.figure(3)
    plt.plot(xx, exact_uu[2,:],'bo')
    plt.plot(xx, numerical_uu[2,:],'r-',label = "energy")
    plt.legend(loc='upper right')
    plt.title('receding forced motion L = %d'%(L))
    plt.show()

    print('L2 error is ', np.linalg.norm(exact_ww - numerical_ww)/np.sqrt(N))



def Result(fluid_info,structure_info, time_info, problem_type, mesh):
    gamma = fluid_info[0]
    [L,N,CFL] = mesh
    dx = float(L)/ float(N)

    [fluid, structure] = FSI_1D(mesh, fluid_info, structure_info, time_info,problem_type)
    print("Final position velocity of the structure are ", structure.qq_n)

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    FS_id = FS_id_n
    xx = np.arange(dx/2, float(L), dx);
    ww = fluid.ww_n
    uu = np.zeros([3,fluid.n])


    for i in range(0,int(FS_id)):

        uu[:,i] = Conser_To_Pri(ww[:,i],[gamma])




    
    plt.figure(figsize=(5,4))
    plt.plot(xx, uu[0,:],'r-',label = "density")
    plt.legend(loc='upper right')
    plt.title('flexible motion L = %d'%(L))
    plt.tight_layout()

    plt.figure(figsize=(5,4))
    plt.plot(xx, uu[1,:],'g-',label = "Velocity")
    plt.legend(loc='upper right')
    plt.xlabel('x')
    plt.ylabel('Velocity')
    plt.ylim([-0.1, 0.7])
    plt.tight_layout()

    plt.figure(figsize=(5,4))
    plt.plot(xx, uu[2,:],'b-',label = "Pressure")
    plt.legend(loc='upper right')
    plt.title('flexible motion L = %d'%(L))
    plt.tight_layout()

    plt.figure(figsize=(5,4))
    plt.plot(xx, abs(uu[1,:]) + np.sqrt(gamma*uu[2,:]/uu[0,:]),'b-',label = "|v| + c")
    plt.legend(loc='upper right')
    plt.title('flexible motion L = %d'%(L))
    plt.tight_layout()

    plt.show()




def Conservation_Values(fluid,structure,eos):

        [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

        FS_id = FS_id_n

        dx = fluid.dx


        ###############################
        # Compute Energy
        ###############################
        energy = 0

        Interpolation_check = np.array([dx, INTERPOLATION_TOL])

        ww = fluid.ww_n

        mass = dx*np.sum(ww[0,0:FS_id])

        energy = dx*np.sum(ww[2,0:FS_id])

        [xs,vs,as_] = structure.qq_n
        print('In Error_check, xs is ', xs)


        structure_energy = structure.ms*vs*vs/2.0 + structure.ks*(xs - structure.L/2.0)*(xs - structure.L/2.0)/2.0

        energy = energy + structure_energy

        x_si = FS_id*dx

        print(" xs is ", xs, " vs is ", vs, " FS_id is ", FS_id, " x_si is ",x_si ,"dx is ", dx)

        #Ghost interpolation

        if(xs > x_si):
            ww_g = Interpolation(ww[:,FS_id - 2], (FS_id - 2)*dx + dx/2, ww[:,FS_id - 1],
                                 (FS_id - 1)*dx + dx/2, FS_id*dx + dx/2, eos, Interpolation_check)
        else:
            ww_g = ww[:,FS_id - 1]

        mass_correction = ww_g[0]*(xs - x_si)

        energy_correction = ww_g[2]*(xs - x_si)

        print("xs - x_si is ", xs- x_si, " mass correction is ", mass_correction," energy correction is ", energy_correction)

        mass = mass + mass_correction

        energy = energy + energy_correction

        return mass, energy





def Conservation_error(fluid_info,structure_info, time_info,test_info, problem_type, mesh):

    Time = time_info[0]

    [N, ite] = test_info

    energy_error = np.zeros(ite)

    mass_error = np.zeros(ite)

    axis_dx = np.zeros(ite)

    O2_check = np.zeros(ite)


    for i in range(ite):

        mesh[1]  = N

        eos = [fluid_info[0]]

        [fluid, structure] = FSI_1D(mesh, fluid_info, structure_info, time_info, problem_type)

        dx = fluid.dx

        print("ite = ", ite, " i = ",i, "dx is ", dx)

        axis_dx[i] = dx

        O2_check[i] =0.10/N**SIV_ORDER

        real_mass = fluid.real_mass

        real_energy = fluid.real_energy

        ###############################
        # Compute Energy
        ###############################
        [mass,energy] = Conservation_Values(fluid, structure, eos)

        mass_error[i] = abs(mass - real_mass)/real_mass

        energy_error[i] = abs(energy - real_energy)/real_energy

        N = 2*N

    print("total mass error = ", mass_error)

    print("total energy error = ", energy_error)




    plt.figure(1)
    plt.loglog(axis_dx, mass_error,'ro-' ,label = "mass error")
    plt.loglog(axis_dx, O2_check,'k--',  label = "O(h^%d)"%(SIV_ORDER))
    plt.grid(True)
    plt.xlabel('dx')
    plt.ylabel('mass_error')
    plt.legend(loc='upper right')
    plt.xlim([axis_dx[0]*3.0, axis_dx[ite - 1]/3.0])
    plt.title('Mass error of flexible motion L = %d'%(fluid.L))

    plt.figure(2)
    plt.loglog(axis_dx, energy_error,'ro-', label = "energy error")
    plt.loglog(axis_dx, O2_check,'k--', label = "O(h^%d)"%(SIV_ORDER))
    plt.grid(True)
    plt.xlabel('dx')
    plt.ylabel('energy_error')
    plt.legend(loc='upper right')
    plt.xlim([axis_dx[0]*3.0, axis_dx[ite - 1]/3.0])

    plt.title('Energy Error of flexible motion L = %d'%(fluid.L))


    plt.show()


