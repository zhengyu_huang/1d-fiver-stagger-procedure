import numpy as np
from scipy.optimize.linesearch import scalar_search_wolfe1, scalar_search_armijo
from scipy.linalg import norm

#The jacobian matrix is like
# A_0 C_1
# B_1 A_1 C_2
#     B_2 A_2 C_3
#             .
#                 .
#                      .
#                     B_n-1 A_n-1

def TriSolver(A,B,C,rhs):


    [n,block_sn,block_sm] = A.shape



    if(block_sm != 3 or block_sn != 3):
        print('matrix dimension error in TriSolver')

    l = block_sn

    '''
    #TriSolver Test
    M = np.zeros([l*n,l*n])
    for i in range(n):
        if(i == 0):
            M[l*i:l*(i+1),l*i:l*(i+1)] = A[i]
            M[l*i:l*(i+1),l*(i+1):l*(i+2)] = C[i+1]
        elif(i == n-1):
            M[l*i:l*(i+1),l*(i-1):l*i] = B[i]
            M[l*i:l*(i+1),l*i:l*(i+1)] = A[i]

        else:
            M[l*i:l*(i+1),l*(i-1):l*i] = B[i]
            M[l*i:l*(i+1),l*i:l*(i+1)] = A[i]
            M[l*i:l*(i+1),l*(i+1):l*(i+2)] = C[i+1]
    rhs_vec = np.reshape(rhs[:,0:n],(l*n,1),order='F')
    '''




    x =np.zeros_like(rhs)

    for i in range(n-1):
        D = np.dot(B[i+1],np.linalg.inv(A[i]))

        A[i+1] = A[i+1] - np.dot(D,C[i+1])

        rhs[:,i+1] = rhs[:,i+1] - np.dot(D,rhs[:,i])

    x[:,n-1] = np.dot(np.linalg.inv(A[n-1]),rhs[:,n-1])

    for i in range(n - 2,-1,-1):

        x[:,i] = np.dot(np.linalg.inv(A[i]), (rhs[:,i] - np.dot(C[i+1],x[:,i+1])))



    
    return x




def _safe_norm(v):
    if not np.isfinite(v).all():
        return np.array(np.inf)
    return norm(v)


def Nonlin_Line_Search(func, x,  Fx, dx, args=(), search_type='armijo', rdiff=1e-8,
                        smin=1e-2):
    tmp_s = [0]
    tmp_Fx = [Fx]
    tmp_phi = [norm(Fx)**2]
    s_norm = norm(x) / norm(dx)

    def phi(s, store=True):
        if s == tmp_s[0]:
            return tmp_phi[0]
        xt = x + s*dx
        
        v = func(xt,*args)
        p = _safe_norm(v)**2
        if store:
            tmp_s[0] = s
            tmp_phi[0] = p
            tmp_Fx[0] = v
        return p

    def derphi(s):
        ds = (abs(s) + s_norm + 1) * rdiff
        return (phi(s+ds, store=False) - phi(s)) / ds

    if search_type == 'wolfe':
        s, phi1, phi0 = scalar_search_wolfe1(phi, derphi, tmp_phi[0],
                                             xtol=1e-2, amin=smin)
    elif search_type == 'armijo':
        s, phi1 = scalar_search_armijo(phi, tmp_phi[0], -tmp_phi[0],
                                       amin=smin)

    if s is None:
        # XXX: No suitable step length found. Take the full Newton step,
        #      and hope for the best.
        s = 1.0

    x = x + s*dx
    if s == tmp_s[0]:
        Fx = tmp_Fx[0]
    else:
        Fx = func(x,*args)
    Fx_norm = norm(Fx)

    return s, x, Fx, Fx_norm

