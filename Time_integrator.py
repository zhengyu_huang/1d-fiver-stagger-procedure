__author__ = 'daniel'


from Space_integrator import *
from solver import *
from copy import deepcopy

'''
def Time_Advance(fluid, structure, time, dt, eos):

    dx = fluid.dx

    Interpolation_check = np.array([dx, INTERPOLATION_TOL])

    xx = fluid.xx

    ############################################
    # Update the fluid state variables
    #####################################################



    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    if(FS_id_n > FS_id_old_n + 1):
        print "CFL number is too large, move over more than one cell in a single step!!!"


    #node 0, node 1, ... node FS_id - 1 are real cells

    ww = fluid.ww_n

    #first step k1 = F(w_{n-1}, xs_{n-1}, vs_{n-1})
    rhs = Compute_Rhs_Muscl(ww, fluid, structure,  1, eos)

    ww_tmp = ww - dt*rhs

    #second step k2 = F(w_{n-1} - dt*k1, xs_n, vs_n)
    #need GTR or RTG in the procedure
    rhs = Compute_Rhs_Muscl(ww_tmp, fluid,  structure,  2, eos)

    ww_tmp = ww_tmp - dt*rhs

    ww = 0.5*(ww_tmp + ww)





    #################################################
    # Ghost to Real Change
    #############################

    if(FS_id_n == FS_id_old_n + 1):
        x_si = 0.5*(xx[FS_id_old_n] + xx[FS_id_old_n + 1])

        [xs, vs, as_] = structure.qq_n

        fluid_normal = 1.0

        ########### Could be optimized UNDO
        ##################
        ww_s = SIV(xx[FS_id_old_n - 2], ww[:, FS_id_old_n - 2], xx[FS_id_old_n - 1], ww[:,FS_id_old_n - 1], x_si, xs, vs, eos, SIV_ORDER, fluid_normal, Interpolation_check)

        ww[:,FS_id_old_n] = Interpolation(ww[:,FS_id_old_n - 1],xx[FS_id_old_n - 1], ww_s, x_si, xx[FS_id_old_n], eos,  Interpolation_check)




    fluid.ww_old_n = fluid.ww_n

    fluid.ww_n = ww

    fluid.t = fluid.t + dt


    ############################################
    # Compute the fluid load
    #####################################################


    ############# no prediction
    [xs, vs, as_] = structure.qq_n

    #compute the load
    fluid_normal = 1.0


    ww_s = SIV(xx[FS_id_n - 2], ww[:,FS_id_n - 2],xx[FS_id_n - 1], ww[:,FS_id_n - 1], xs, xs, vs, eos, SIV_ORDER, fluid_normal, Interpolation_check)

    uu_s = Conser_To_Pri(ww_s,eos)

    f = uu_s[2]

    ############################################
    # Update the position, velocity and acceleration of the structure
    #####################################################

    if(structure.t < time - 1e-8):

        [xs_old_n, vs_old_n, as_old_n] = structure.qq_old_n

        as_ = (f - structure.ks*(xs_old_n - structure.L/2 + dt *vs_old_n)) /(structure.ms + dt*dt* structure.ks)


        structure.qq_old_n = structure.qq_n

        structure.qq_old_n[2] = as_

        structure.qq_n = np.array([xs_old_n + 2.0*dt*vs_old_n + 2*dt*dt * as_, vs_old_n + 2*dt*as_, 0])
        #Because we do not use acceleration, set it as default value 0

        structure.t = structure.t + dt
'''



def A6_First_Step(fluid, structure, time, dt, eos,problem_type):


    forced_or_aero = problem_type[2]



    if(forced_or_aero == 'aero'):
        #assuming constant initial condition
        w = fluid.ww_n[:,0]

        u = Conser_To_Pri(w,eos)

        force = u[2]

        [xs_0,vs_0,as_0] = structure.qq_n

        as_0 = force/structure.ms

        # there just predict the qq_{1/2}

        xs = xs_0 + dt*vs_0 + dt*dt*as_0/2.0

        vs = vs_0 + dt*as_0

        structure.qq_n = np.array([xs,vs,0])

        structure.qq_old_n = np.array([xs_0,vs_0,as_0])

        structure.qq_h = np.array([xs_0,vs_0,as_0])
    elif(forced_or_aero == 'forced'):


        structure.qq_old_n = structure.qq_n

        structure.qq_h = structure.qq_n

        structure.t = structure.t + dt

        structure.qq_n = structure.Forced_Motion(structure.t)




    Fluid_Time_Advance(fluid, structure, time, dt, eos,problem_type)












def Fluid_Time_Advance(fluid, structure, time, dt, eos,problem_type):

    dx = fluid.dx

    Interpolation_check = np.array([dx, INTERPOLATION_TOL])

    xx = fluid.xx

    ############################################
    # Update the fluid state variables
    #####################################################



    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    if(FS_id_n > FS_id_old_n + 1):
        print("CFL number is too large, move over more than one cell in a single step!!!")


    #node 0, node 1, ... node FS_id - 1 are real cells

    implicit_or_explicit = problem_type[1]

    if(implicit_or_explicit == 'explicit'):


        ww = fluid.ww_n

        #first step k1 = F(w_{n-1}, xs_{n-1}, vs_{n-1})
        rhs = Compute_Rhs_Muscl(ww, fluid, structure,  1, eos)

        ww_tmp = ww - dt*rhs

        #second step k2 = F(w_{n-1} - dt*k1, xs_n, vs_n)
        #need GTR or RTG in the procedure
        rhs = Compute_Rhs_Muscl(ww_tmp, fluid,  structure,  2, eos)

        ww_tmp = ww_tmp - dt*rhs

        ww = 0.5*(ww_tmp + ww)





        #################################################
        # Ghost to Real Change
        #############################

        if(FS_id_n == FS_id_old_n + 1):
            x_si = 0.5*(xx[FS_id_old_n] + xx[FS_id_old_n + 1])

            [xs, vs, as_] = structure.qq_n

            fluid_normal = 1.0

            ########### Could be optimized UNDO
            ##################
            ww_s = SIV(xx[FS_id_old_n - 2], ww[:, FS_id_old_n - 2], xx[FS_id_old_n - 1], ww[:,FS_id_old_n - 1], x_si, xs, vs, eos, SIV_ORDER, fluid_normal, Interpolation_check)

            ww[:,FS_id_old_n] = Interpolation(ww[:,FS_id_old_n - 1],xx[FS_id_old_n - 1], ww_s, x_si, xx[FS_id_old_n], eos,  Interpolation_check)




        fluid.ww_old_n = fluid.ww_n

        fluid.ww_n = ww

        fluid.t = fluid.t + dt

    elif(implicit_or_explicit == 'implicit'):


        print('implicit')
        u = deepcopy(fluid.ww_n)

        #Update fluid by 3PBDF, assume only node 0, node 1,... node FS_id_old_n -1 are real cells
        newton_ite = 0;

        rhs = Compute_Rhs_Muscl_Implicit(u, fluid, structure,dt)
        ref = np.linalg.norm(rhs)

        while True:
            newton_ite = newton_ite + 1;
            rhs = Compute_Rhs_Muscl_Implicit(u, fluid, structure,dt)

            A,B,C = Compute_Jacobian(u,fluid,structure,dt)


            #dw = - J^{-1} rhs
            dw = -TriSolver(A,B,C, rhs)



            [s, x, Fx, Fx_norm] = Nonlin_Line_Search(Compute_Rhs_Muscl_Implicit, u,  rhs, dw,(fluid,structure,dt))
            #print 'residual is ', Fx_norm


            u = u + s*dw

            if (Fx_norm < 1.0e-3*ref or newton_ite > 100):
                print('residual is', np.linalg.norm(Compute_Rhs_Muscl_Implicit(u, fluid, structure,dt)))
                break


        print('newton iteration time  is', newton_ite)

        fluid.ww_old_n = fluid.ww_n

        fluid.ww_n = deepcopy(u)


        if(FS_id_n != FS_id_old_n):

            fluid.PhaseChangeUpdate(structure, 1)





        #fluid.update(structure,2) #Algorithm 2#



        fluid.t = fluid.t + dt

        #structure.t = structure.t + dt
        print('structure.t is ', structure.t)
        print('fluid.t is ', fluid.t)



    ############################################
    # Compute the fluid load
    #####################################################




def Structure_Time_Advance(fluid, structure, time, dt, eos, problem_type):

    [method,implicit_or_explicit, forced_or_aero] = problem_type

    xx = fluid.xx

    ww = fluid.ww_n

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    Interpolation_check = fluid.Interpolation_check

    #use predictor

    [xs, vs, as_] = structure.qq_n

    #compute the load
    fluid_normal = 1.0


    ww_s = SIV(xx[FS_id_n - 2], ww[:,FS_id_n - 2],xx[FS_id_n - 1], ww[:,FS_id_n - 1], xs, xs, vs, eos, SIV_ORDER, fluid_normal, Interpolation_check)

    uu_s = Conser_To_Pri(ww_s,eos)

    f = uu_s[2]

    ############################################
    # Update the position, velocity and acceleration of the structure
    #####################################################

    if(structure.t < time - 1e-8):
        if(forced_or_aero == 'aero'):

            if(method == 'A6'):

                [xs_old_n, vs_old_n, as_old_n] = structure.qq_h

                as_ = (f - structure.ks*(xs_old_n - structure.L/2 + dt *vs_old_n/2)) /(structure.ms + dt*dt* structure.ks/4)

                structure.qq_old_n = structure.qq_n

                vs = vs_old_n + dt*as_

                xs = xs_old_n + dt*(vs_old_n + vs)/2

                structure.qq_h = np.array([xs,vs, 0])

                structure.qq_n = np.array([xs + 0.5*dt*vs + 0.125*dt*(vs - vs_old_n) ,1.5*vs - 0.5*vs_old_n,0 ])
                #Because we do not use acceleration, set it as default value 0

                structure.t = structure.t + dt

            elif(method == 'A7'):

                [xs_old_n, vs_old_n, as_old_n] = structure.qq_old_n

                as_ = (f - structure.ks*(xs_old_n - structure.L/2 + dt *vs_old_n)) /(structure.ms + dt*dt* structure.ks)


                structure.qq_old_n = structure.qq_n

                structure.qq_old_n[2] = as_

                structure.qq_n = np.array([xs_old_n + 2.0*dt*vs_old_n + 2*dt*dt * as_, vs_old_n + 2*dt*as_, 0])
                #Because we do not use acceleration, set it as default value 0

                structure.t = structure.t + dt

        elif(forced_or_aero == 'forced'):

            structure.qq_h = structure.Forced_Motion(structure.t + dt/2)

            structure.t = structure.t + dt

            structure.qq_old_n = structure.qq_n

            structure.qq_n = structure.Forced_Motion(structure.t)

         


    print('time is', structure.t, 'dt is ',dt)







    return 0


'''
def Structure_Time_Advance_Explicit(fluid, structure, time, dt, eos):

    xx = fluid.xx

    ww = fluid.ww_n

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    Interpolation_check = fluid.Interpolation_check

    #use predictor

    [xs, vs, as_] = structure.qq_n

    #compute the load
    fluid_normal = 1.0


    ww_s = SIV(xx[FS_id_n - 2], ww[:,FS_id_n - 2],xx[FS_id_n - 1], ww[:,FS_id_n - 1], xs, xs, vs, eos, SIV_ORDER, fluid_normal, Interpolation_check)

    uu_s = Conser_To_Pri(ww_s,eos)

    f = uu_s[2]

    ############################################
    # Update the position, velocity and acceleration of the structure
    #####################################################

    if(structure.t < time - 1e-8):

        [xs_old_n, vs_old_n, as_old_n] = structure.qq_h

        as_ = (f - structure.ks*(xs_old_n - structure.L/2 + dt *vs_old_n/2)) /(structure.ms + dt*dt* structure.ks/4)

        structure.qq_old_n = structure.qq_n

        vs = vs_old_n + dt*as_

        xs = xs_old_n + dt*(vs_old_n + vs)/2

        structure.qq_h = np.array([xs,vs, 0])

        structure.qq_n = np.array([xs + 0.5*dt*vs + 0.125*dt*(vs - vs_old_n) ,1.5*vs - 0.5*vs_old_n,0 ])
        #Because we do not use acceleration, set it as default value 0

        structure.t = structure.t + dt

'''






'''
def Fluid_Time_Advance_Implicit(fluid, structure, time, dt, eos):

    u = deepcopy(fluid.ww_n)

    dx = fluid.dx

    Interpolation_check = np.array([dx, INTERPOLATION_TOL])

    xx = fluid.xx

    ############################################
    # Update the fluid state variables
    #####################################################





    t = structure.t + dt




    #structure.qq_old_n = deepcopy(structure.qq_n)

    #structure.Forced_Motion(t)



    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )
    print FS_id_n

    if(FS_id_n > FS_id_old_n + 1):
        print "CFL number is too large, move over more than one cell in a single step!!!"
    #node 0, node 1, ... node FS_id - 1 are real cells


    #Update fluid by 3PBDF, assume only node 0, node 1,... node FS_id_old_n -1 are real cells
    while True:
        rhs = Compute_Rhs_Muscl(u, fluid, structure,dt)

        A,B,C = Compute_Jacobian(u,fluid,structure,dt)


        #dw = - J^{-1} rhs
        dw = -TriSolver(A,B,C, rhs)



        [s, x, Fx, Fx_norm] = Nonlin_Line_Search(Compute_Rhs_Muscl, u,  rhs, dw,(fluid,structure,dt))
        #print 'residual is ', Fx_norm


        u = u + s*dw

        if (Fx_norm < 1.0e-8):
            print 'residual is', np.linalg.norm(Compute_Rhs_Muscl(u, fluid, structure,dt))
            break




    fluid.ww_old_n = fluid.ww_n

    fluid.ww_n = deepcopy(u)


    if(FS_id_n != FS_id_old_n):

        fluid.PhaseChangeUpdate(structure, 1)





    #fluid.update(structure,2) #Algorithm 2#



    fluid.t = fluid.t + dt

    #structure.t = structure.t + dt
    print 'structure.t is ', structure.t
    print 'fluid.t is ', fluid.t

'''









def Max_Wave_Speed(structure, fluid, eos):

    [gamma] = eos

    v_max = 0

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    FS_id = FS_id_n

    ww = fluid.ww_n

    uu = np.zeros([3,fluid.n])

    for i in range(0,int(FS_id)):

        uu[:,i] = Conser_To_Pri(ww[:,i],[gamma])

    for i in range(int(FS_id)):

        uu[:,i] = Conser_To_Pri(ww[:,i],[gamma])
        v = abs(uu[1,i]) + np.sqrt(gamma*uu[2,i]/uu[0,i])

        v_max = max(v,v_max)

    return v_max

