__author__ = 'daniel'

import numpy as np

global SIV_ORDER
global INTERPOLATION_TOL
SIV_ORDER = 2
INTERPOLATION_TOL = 0.1


def Pri_To_Conser(fluid,eos):
    [gamma] = eos
    [rho, v, p] = fluid;
    return  np.array([rho, rho*v, rho*v*v/2 + p/(gamma - 1)])


def Conser_To_Pri(fluid, gas):
    [gamma] = gas
    [w1, w2, w3] = fluid
    rho = w1;
    v = w2/w1;
    p = (w3 - w2*v/2) * (gamma - 1)
    return np.array([rho, v, p])


def Flux(ww, eos):
    u = Conser_To_Pri(ww, eos)
    [gamma] = eos
    [rho, v, p] = u
    return np.array([rho*v, rho*v*v +p, (rho*v*v/2 + gamma * p/(gamma - 1))*v])

def Interpolation(w1, x1, w2, x2, x3, eos, check):
    [dx,tol] = check
    u1 = Conser_To_Pri(w1, eos)
    u2 = Conser_To_Pri(w2, eos)

    if(abs(x2 - x1) > tol*dx):
        u3 = (u2 - u1) * (x3 - x2)/(x2 - x1) + u2
        if(u3[0] >= 0 and u3[2] >= 0):
            w3 =Pri_To_Conser(u3,eos)
            return w3
        else:
            print("w1 is ", w1, " w2 is ", w2)
            print("In Interpolation, encounter negative values ")
    else:
        print("In Interpolation, points are too closed")

    if(abs(x1 - x3) > abs(x2 -x3)):
        return w2
    else:
        return w1


def Limiter_Fun(r):
    f =np.array([0.,0.,0.])
    for i in range(3):
        if(r[i] > 0):
            f[i] = 2*r[i]/(r[i] + 1)
        else:
            f[i] = 0
    return f



# this is Fluid Fluid Roe Flux function
def FF_Roe_Flux(ww_l, ww_r, eos):
    u_l = Conser_To_Pri(ww_l,eos)
    u_r = Conser_To_Pri(ww_r,eos)
    [gamma] = eos;
    [rho_l, v_l, p_l] = u_l;
    [rho_r, v_r, p_r] = u_r;

    c_l = np.sqrt(gamma * p_l/ rho_l)
    c_r = np.sqrt(gamma * p_r/ rho_r)
    w_l = np.array([rho_l, rho_l*v_l, rho_l*v_l*v_l/2 + p_l/(gamma - 1)])
    w_r = np.array([rho_r, rho_r*v_r, rho_r*v_r*v_r/2 + p_r/(gamma - 1)])
    h_l = v_l*v_l/2.0 + gamma * p_l/(rho_l * (gamma - 1));
    h_r = v_r*v_r/2.0 + gamma * p_r/(rho_r * (gamma - 1));

    # compute the Roe-averaged quatities
    v_rl = (np.sqrt(rho_l)*v_l + np.sqrt(rho_r)*v_r)/ (np.sqrt(rho_r) + np.sqrt(rho_l));
    h_rl = (np.sqrt(rho_l)*h_l + np.sqrt(rho_r)*h_r)/ (np.sqrt(rho_r) + np.sqrt(rho_l));
    c_rl = np.sqrt((gamma - 1)*(h_rl - v_rl * v_rl/2));
    rho_rl = np.sqrt(rho_r * rho_l);


    du = v_r - v_l
    dp = p_r - p_l
    drho = rho_r - rho_l
    du1 = du + dp/(rho_rl * c_rl);
    du2 = drho - dp/(c_rl * c_rl);
    du3 = du - dp/(rho_rl * c_rl);

    #compute the Roe-average wave speeds
    lambda_1 = v_rl + c_rl;
    lambda_2 = v_rl;
    lambda_3 = v_rl - c_rl;

    #compute the right characteristic vectors
    r_1 =  rho_rl/(2 * c_rl) * np.array([1 ,v_rl + c_rl, h_rl + c_rl * v_rl]);
    r_2 = np.array([1, v_rl, v_rl * v_rl/2]);
    r_3 = -rho_rl/(2 * c_rl) * np.array([1,v_rl - c_rl, h_rl - c_rl * v_rl]);


    #compute the fluxes
    mach = v_rl/ c_rl
    if(mach <=-1):
        flux = np.array([rho_r*v_r, rho_r*v_r*v_r + p_r, rho_r*v_r*h_r])
    elif(mach <= 0 and mach >= -1):
        flux = np.array([rho_r * v_r, rho_r*v_r**2 + p_r,rho_r*h_r*v_r]) - r_1*lambda_1*du1;
    elif(mach >= 0 and mach <= 1):
        flux = np.array([rho_l * v_l, rho_l*v_l**2 + p_l,rho_l*h_l*v_l]) + r_3*lambda_3*du3;
    else:
        flux = np.array([rho_l * v_l, rho_l*v_l**2 + p_l,rho_l*h_l*v_l])

    return flux

def FF_Roe_Jacobian(ww_l, ww_r, eos):
    u_l = Conser_To_Pri(ww_l,eos)
    u_r = Conser_To_Pri(ww_r,eos)
    [gamma] = eos;
    [rho_l, v_l, p_l] = u_l;
    [rho_r, v_r, p_r] = u_r;

    c_l = np.sqrt(gamma * p_l/ rho_l)
    c_r = np.sqrt(gamma * p_r/ rho_r)
    w_l = np.array([rho_l, rho_l*v_l, rho_l*v_l*v_l/2 + p_l/(gamma - 1)])
    w_r = np.array([rho_r, rho_r*v_r, rho_r*v_r*v_r/2 + p_r/(gamma - 1)])
    h_l = v_l*v_l/2.0 + gamma * p_l/(rho_l * (gamma - 1));
    h_r = v_r*v_r/2.0 + gamma * p_r/(rho_r * (gamma - 1));

    # compute the Roe-averaged quatities
    v_rl = (np.sqrt(rho_l)*v_l + np.sqrt(rho_r)*v_r)/ (np.sqrt(rho_r) + np.sqrt(rho_l));
    h_rl = (np.sqrt(rho_l)*h_l + np.sqrt(rho_r)*h_r)/ (np.sqrt(rho_r) + np.sqrt(rho_l));
    c_rl = np.sqrt((gamma - 1)*(h_rl - v_rl * v_rl/2));
    rho_rl = np.sqrt(rho_r * rho_l);
    p_rl = rho_rl*c_rl**2/gamma

    du = v_r - v_l
    dp = p_r - p_l
    drho = rho_r - rho_l
    du1 =  du*(rho_rl * c_rl) + dp;

    du3 = du*(rho_rl * c_rl) - dp;

    #compute the Roe-average wave speeds
    lambda_1 = v_rl + c_rl;

    lambda_3 = v_rl - c_rl;

    #compute the right characteristic vectors
    r_1 =  1/(2 * c_rl**2) * np.matrix([[1], [v_rl + c_rl], [h_rl + c_rl * v_rl]]);

    r_3 =  -1/(2 * c_rl**2) * np.matrix([[1], [v_rl - c_rl], [h_rl - c_rl * v_rl]]);




    #A_l = dF(w_l)/dw_l
    A_l = np.array([[0,1.0,0.0],[v_l*v_l*(gamma - 3)/2, (3 - gamma)*v_l, gamma - 1],[(gamma-1)*v_l**3/2 - v_l*h_l, h_l -(gamma-1)*v_l**2, gamma*v_l]])




    ##compute du_rl_over_du_l
    a = 1/(np.sqrt(rho_l)*(np.sqrt(rho_l) + np.sqrt(rho_r)))
    ##d(rho_rl,v_rl,p_rl)/d(rho_rl,v_rl,h_rl)
    du_rl_du_hrl = np.matrix([[1.,0.,0.],
                         [0.,1.,0.],
                         [p_rl/rho_rl, -(gamma-1)*rho_rl*v_rl/gamma, (gamma -1)*rho_rl/gamma]])

    du_hrl_du_l = np.matrix([[np.sqrt(rho_r/rho_l)/2, 0,0],
                        [0.5*(v_l - v_rl)/(rho_l + np.sqrt(rho_l*rho_r)), np.sqrt(rho_l)/(np.sqrt(rho_l) + np.sqrt(rho_r)),0],
                        [-a**2*(1 + 0.5*np.sqrt(rho_r/rho_l))*gamma*p_l/(gamma -1) + 0.25*np.sqrt(rho_r/rho_l)*(v_l**2 -2* h_r)/(np.sqrt(rho_r) + np.sqrt(rho_l))**2, a*rho_l*v_l, a*gamma/(gamma - 1)]])

    du_rl_du_l = du_rl_du_hrl*du_hrl_du_l


    dr1_du_rl = np.matrix([[1/(2*gamma*p_rl),0,-rho_rl/(2*gamma*p_rl**2) ],
                          [v_rl/(2*gamma*p_rl) + 1/(4*c_rl*rho_rl),1/(2*c_rl**2),- v_rl/(2*c_rl**2*p_rl) - 1/(4*p_rl*c_rl)],
                          [v_rl**2/(4*gamma*p_rl) + v_rl/(4*c_rl*rho_rl), v_rl/(2*c_rl**2) + 1/(2*c_rl), - v_rl**2/(4*p_rl*c_rl**2) - v_rl/(4*p_rl*c_rl)]])

    dr3_du_rl = -np.matrix([[1/(2*gamma*p_rl),0,-rho_rl/(2*gamma*p_rl**2) ],
                          [v_rl/(2*gamma*p_rl) - 1/(4*c_rl*rho_rl),1/(2*c_rl**2),- v_rl/(2*c_rl**2*p_rl) + 1/(4*p_rl*c_rl)],
                          [v_rl**2/(4*gamma*p_rl) - v_rl/(4*c_rl*rho_rl), v_rl/(2*c_rl**2) - 1/(2*c_rl), - v_rl**2/(4*p_rl*c_rl**2) + v_rl/(4*p_rl*c_rl)]])


    dlambda_1_du_rl = np.matrix([-c_rl/(2*rho_rl),1,c_rl/(2*p_rl)])

    dlambda_3_du_rl = np.matrix([+c_rl/(2*rho_rl),1,-c_rl/(2*p_rl)])




    [w1,w2,w3] = w_l
    du_l_dw_l = np.matrix([[1,0,0],[-w2/w1**2,1/w1,0],[w2**2/w1**2*(gamma - 1)/2, -w2/w1*(gamma - 1), gamma - 1]])

    # B = drho_rl*c_rl/du_l
    B = np.matrix([[c_rl/2.0,0,gamma/(2*c_rl)]])*du_rl_du_l
    du1_du_l = np.matrix([(v_r-v_l)*B[0,0],  -rho_rl*c_rl + (v_r - v_l)*B[0,1], -1 +(v_r-v_l)*B[0,2]])
    du3_du_l = np.matrix([(v_r-v_l)*B[0,0],  -rho_rl*c_rl + (v_r - v_l)*B[0,1], 1 +(v_r-v_l)*B[0,2]])







    #A_l = dF(w_r)/dw_r
    A_r = np.array([[0,1.0,0.0],[v_r*v_r*(gamma - 3)/2, (3 - gamma)*v_r, gamma - 1],[(gamma-1)*v_r**3/2 - v_r*h_r, h_r -(gamma-1)*v_r**2, gamma*v_r]])




    ##compute du_rl_over_du_l
    a = 1/(np.sqrt(rho_r)*(np.sqrt(rho_l) + np.sqrt(rho_r)))
    ##d(rho_rl,v_rl,p_rl)/d(rho_rl,v_rl,h_rl)
    du_rl_du_hrl = np.matrix([[1.,0.,0.],
                         [0.,1.,0.],
                         [p_rl/rho_rl, -(gamma-1)*rho_rl*v_rl/gamma, (gamma -1)*rho_rl/gamma]])

    du_hrl_du_r = np.matrix([[np.sqrt(rho_l/rho_r)/2, 0,0],
                        [0.5*(v_r - v_rl)/(rho_r + np.sqrt(rho_l*rho_r)), np.sqrt(rho_r)/(np.sqrt(rho_l) + np.sqrt(rho_r)),0],
                        [-a**2*(1 + 0.5*np.sqrt(rho_l/rho_r))*gamma*p_r/(gamma -1) + 0.25*np.sqrt(rho_l/rho_r)*(v_r**2 -2* h_l)/(np.sqrt(rho_r) + np.sqrt(rho_l))**2, a*rho_r*v_r, a*gamma/(gamma - 1)]])

    du_rl_du_r = du_rl_du_hrl*du_hrl_du_r


    dr1_du_rl = np.matrix([[1/(2*gamma*p_rl),0,-rho_rl/(2*gamma*p_rl**2) ],
                          [v_rl/(2*gamma*p_rl) + 1/(4*c_rl*rho_rl),1/(2*c_rl**2),- v_rl/(2*c_rl**2*p_rl) - 1/(4*p_rl*c_rl)],
                          [v_rl**2/(4*gamma*p_rl) + v_rl/(4*c_rl*rho_rl), v_rl/(2*c_rl**2) + 1/(2*c_rl), - v_rl**2/(4*p_rl*c_rl**2) - v_rl/(4*p_rl*c_rl)]])

    dr3_du_rl = -np.matrix([[1/(2*gamma*p_rl),0,-rho_rl/(2*gamma*p_rl**2) ],
                          [v_rl/(2*gamma*p_rl) - 1/(4*c_rl*rho_rl),1/(2*c_rl**2),- v_rl/(2*c_rl**2*p_rl) + 1/(4*p_rl*c_rl)],
                          [v_rl**2/(4*gamma*p_rl) - v_rl/(4*c_rl*rho_rl), v_rl/(2*c_rl**2) - 1/(2*c_rl), - v_rl**2/(4*p_rl*c_rl**2) + v_rl/(4*p_rl*c_rl)]])


    dlambda_1_du_rl = np.matrix([-c_rl/(2*rho_rl),1,c_rl/(2*p_rl)])

    dlambda_3_du_rl = np.matrix([+c_rl/(2*rho_rl),1,-c_rl/(2*p_rl)])




    [w1,w2,w3] = w_r
    du_r_dw_r = np.matrix([[1,0,0],[-w2/w1**2,1/w1,0],[w2**2/w1**2*(gamma - 1)/2, -w2/w1*(gamma - 1), gamma - 1]])

    # B = drho_rl*c_rl/du_r
    B = np.matrix([[c_rl/2.0,0,gamma/(2*c_rl)]])*du_rl_du_r

    du1 =  du*(rho_rl * c_rl) + dp;

    du3 = du*(rho_rl * c_rl) - dp;


    du1_du_r = np.matrix([(v_r-v_l)*B[0,0],  rho_rl*c_rl + (v_r - v_l)*B[0,1], 1 +(v_r-v_l)*B[0,2]])

    du3_du_r = np.matrix([(v_r-v_l)*B[0,0],  rho_rl*c_rl + (v_r - v_l)*B[0,1], -1 +(v_r-v_l)*B[0,2]])





    mach = v_rl/ c_rl
    if(mach <=-1):
        jac_l = np.zeros([3,3])
        jac_r = A_r
    elif(mach <= 0 and mach >= -1):
        jac_l = -lambda_1*du1*dr1_du_rl*du_rl_du_l*du_l_dw_l - du1*r_1*dlambda_1_du_rl*du_rl_du_l*du_l_dw_l - lambda_1*r_1*du1_du_l*du_l_dw_l
        jac_r = A_r - lambda_1*du1*dr1_du_rl*du_rl_du_r*du_r_dw_r - du1*r_1*dlambda_1_du_rl*du_rl_du_r*du_r_dw_r - lambda_1*r_1*du1_du_r*du_r_dw_r
    elif(mach >= 0 and mach <= 1):
        jac_l = A_l + lambda_3*du3*dr3_du_rl*du_rl_du_l*du_l_dw_l + du3*r_3*dlambda_3_du_rl*du_rl_du_l*du_l_dw_l + lambda_3*r_3*du3_du_l*du_l_dw_l
        jac_r = lambda_3*du3*dr3_du_rl*du_rl_du_r*du_r_dw_r + du3*r_3*dlambda_3_du_rl*du_rl_du_r*du_r_dw_r + lambda_3*r_3*du3_du_r*du_r_dw_r
    else:
        jac_l = A_l
        jac_r = np.zeros([3,3])
    return jac_l, jac_r





def SIV(x1, ww1,x2, ww2, x_si, x_wall, v_wall, eos, Order, fluid_normal, Interpolation_check):
    [dx, tol] = Interpolation_check
    ##########
    #1 order
    ########################
    if(Order == 1):
        if(np.fabs(x1 - x_si) > np.fabs(x2 - x_si)):
            ww_e = ww2
        else:
            ww_e = ww1

        ww_bs = FSRiemann(ww_e, v_wall,fluid_normal, eos)

        ww_si = ww_bs

    ###########
    #2 order
    ###########################
    else:

        ww_e = Interpolation(ww1, x1, ww2, x2, x_wall,eos, Interpolation_check)

        #ww_qq = FSRiemann(ww1, v_wall,fluid_normal, eos)
        ww_bs = FSRiemann(ww_e, v_wall,fluid_normal, eos)

        #ww_si = Interpolation(ww1, x1, ww_bs, x_wall, x_si,eos, Interpolation_check)


        if(x_wall > x_si):
            ww_si = Interpolation(ww2, x2, ww_bs, x_wall, x_si,eos, Interpolation_check)
        else:
            ww_si = Interpolation(ww1, x1, ww_bs, x_wall, x_si,eos, Interpolation_check)


    return ww_si





#This is 1-d case
# w_l is the fluid state variable
# v is the piston or wall speed
# fluid_normal is the normal of the fluid interface
# if fluid_normal = 1 piston is on the right
# if fluid_normal =-1 piston is on the left
def FSRiemann(w_l, v, fluid_normal, eos):
    u_l = Conser_To_Pri(w_l, eos)
    [gamma] = eos;
    [rho_l, v_l, p_l] = u_l;
    #left facing shock case
#    if(fluid_normal < 0 and np.fabs(v_l) > 0.01):
#        a = 1
    if(v_l*fluid_normal > v*fluid_normal):
        #print "Shock Wave FSRiemann"
        a = 2/((gamma + 1)*rho_l);
        b = p_l*(gamma - 1)/(gamma + 1)
        phi = a/(v - v_l)**2

        p = p_l + (1 + np.sqrt(4*phi*(p_l + b) + 1))/(2*phi)
        rho = rho_l*(p/p_l + (gamma - 1)/(gamma + 1))/(p/p_l * (gamma - 1)/(gamma + 1) + 1)
    #left facing rarefactions case
    else:
        #print "Rarefactions FSRiemann"
        c_l = np.sqrt(gamma*p_l/rho_l);
        p = p_l*(-(gamma - 1)/(2*c_l)*(v - v_l)*fluid_normal + 1)**(2*gamma/(gamma - 1))
        rho = rho_l*(p/p_l)**(1/gamma);
    u_s = np.array([rho, v, p]);
    w_s = Pri_To_Conser(u_s, eos)
    return w_s



#assume fluid norm is 1
def FSRiemann_Jacobian(w_l, v, eos):
    u_l = Conser_To_Pri(w_l, eos)
    [gamma] = eos;
    [rho_l, v_l, p_l] = u_l;

    #left facing shock case
    #    if(fluid_normal < 0 and np.fabs(v_l) > 0.01):
    #        a = 1
    if(v_l > v):
        #print "Shock Wave FSRiemann"
        a = 2/((gamma + 1)*rho_l);
        b = p_l*(gamma - 1)/(gamma + 1)
        phi = a/(v - v_l)**2

        p = p_l + (1 + np.sqrt(4*phi*(p_l + b) + 1))/(2*phi)
        rho = rho_l*(p/p_l + (gamma - 1)/(gamma + 1))/(p/p_l * (gamma - 1)/(gamma + 1) + 1)


        dp_d_phi = (2*gamma*p_l/(gamma + 1))/(phi*np.sqrt(4*phi*(p_l + b) + 1)) - (1 + np.sqrt(4*phi*(p_l + b) + 1))/(2*phi**2)

        jac = np.matrix([[0.,0.,0.],[0.,0.,0.],[-dp_d_phi*phi/rho_l, dp_d_phi*2*phi/(v - v_l), 1 + 2*gamma/((gamma + 1)*np.sqrt(4*phi*(p_l + b) + 1))]])

        drho_drho_l = rho/rho_l
        drho_dp = rho_l*p_l*4*gamma/(p*(gamma-1) + p_l*(gamma+1))**2
        drho_dp_l = rho_l*p*(-4*gamma)/(p*(gamma-1)+p_l*(gamma+1))**2

        jac[0,0] = drho_drho_l + drho_dp*jac[2,0]
        jac[0,1] = drho_dp*jac[2,1]
        jac[0,2] = drho_dp_l + drho_dp*jac[2,2]



    #left facing rarefactions case
    else:
        #print "Rarefactions FSRiemann"
        c_l = np.sqrt(gamma*p_l/rho_l);
        p = p_l*(-(gamma - 1)/(2*c_l)*(v - v_l) + 1)**(2*gamma/(gamma - 1))
        rho = rho_l*(p/p_l)**(1/gamma);


        dp_dc_l = p_l* (-(gamma - 1)/(2*c_l)*(v - v_l) + 1)**((gamma + 1)/(gamma - 1)) *gamma*(v - v_l)/c_l**2
        dp_dv_l = p_l* (-(gamma - 1)/(2*c_l)*(v - v_l) + 1)**((gamma + 1)/(gamma - 1)) *gamma/c_l
        dp_dp_l = (-(gamma - 1)/(2*c_l)*(v - v_l) + 1)**(2*gamma/(gamma - 1))
        dc_l_dp_l = c_l/(2*p_l)
        dc_l_drho_l = -c_l/(2*rho_l)
        jac = np.matrix([[0.,0.,0.],[0.,0.,0.],[dp_dc_l*dc_l_drho_l, dp_dv_l,dp_dp_l + dp_dc_l*dc_l_dp_l]])

        drho_drho_l = (p/p_l)**(1/gamma)
        drho_dp = rho/(p*gamma)
        drho_dp_l = -rho/(p_l*gamma)

        jac[0,0] = drho_drho_l + drho_dp*jac[2,0]
        jac[0,1] = drho_dp*jac[2,1]
        jac[0,2] = drho_dp_l + drho_dp*jac[2,2]



    [w1,w2,w3] = w_l

    du_l_dw_l = np.matrix([[1,0,0],[-w2/w1**2,1/w1,0],[w2**2/w1**2*(gamma - 1)/2, -w2/w1*(gamma - 1), gamma - 1]])


    jac = jac*du_l_dw_l

    return jac




################################################
# Fluid Class
###########################################################

class Fluid:
    def __init__(self, N, L):
        dx = float(L)/ float(N)
        self.dx = dx
        self.n = N
        self.L = L
        self.t = 0
        self.xx = np.arange(dx/2, L, dx)
        self.ww_n = np.zeros([3,N])
        self.ww_old_n = np.zeros([3,N])
        self.Interpolation_check = np.array([dx, INTERPOLATION_TOL])


        self.real_mass = 0
        self.real_momentum = 0
        self.real_energy  = 0

    def Const_initial(self, fluid_info):
        [gamma, rho_l, v_l, p_l] = fluid_info
        eos = [gamma]
        fluid = np.array([rho_l, v_l, p_l]);
        for i in range(int(self.n)):
            if(self.xx[i] <= self.L/2.0):
                uu = Pri_To_Conser(fluid, eos)

                self.ww_n[:,i]  = uu
                self.ww_old_n[:,i] = uu

        self.eos = eos
        self.real_mass = rho_l*0.5*self.L
        self.real_momentum = v_l*rho_l*0.5*self.L
        self.real_energy  = (0.5*rho_l*v_l*v_l + p_l/(gamma -1))*0.5*self.L


    def Linear_initial(self, fluid_info_l, fluid_info_r): ####UODO ENERGY
        [gamma, rho_l, v_l, p_l] = fluid_info_l
        [gamma, rho_r, v_r, p_r] = fluid_info_r


        fluid_l = np.array([rho_l, v_l, p_l])
        fluid_r = np.array([rho_r, v_r, p_r])
        eos = [gamma]


        for i in range(self.n):
            if(self.xx[i] <= self.L/2.0):
                fluid = fluid_l + self.xx[i] * (fluid_r - fluid_l)/(self.L/2.0)
                self.ww_n[:,i] = Pri_To_Conser(fluid, eos)

        self.real_mass = (rho_l + rho_r)/2    *  self.L/ 2.0
        self.real_momentum = (p_r + p_l)/2 * self.L/2.0
        self.real_energy = 0


    def PhaseChangeUpdate(self, structure, alg):
        [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

        if FS_id_n == FS_id_old_n:

            return

        [xs,vs,as_0] = structure.qq_n
        [xs_old,vs_old,as_old] = structure.qq_old_n

        if alg == 1:
            #Using interpolation between inside node and Riemann solution
            xx = self.xx

            x_si = 0.5*(xx[FS_id_old_n] + xx[FS_id_old_n + 1])

            fluid_normal = 1.0

            ww= self.ww_old_n

            ww_s = SIV(xx[FS_id_old_n - 2], ww[:, FS_id_old_n - 2], xx[FS_id_old_n - 1], ww[:,FS_id_old_n - 1], x_si, xs_old, vs_old, self.eos, SIV_ORDER, fluid_normal, self.Interpolation_check)

            ww[:,FS_id_old_n] = Interpolation(ww[:,FS_id_old_n - 1],xx[FS_id_old_n - 1], ww_s, x_si, xx[FS_id_old_n], self.eos,  self.Interpolation_check)


            ww= self.ww_n

            ww_s = SIV(xx[FS_id_old_n - 2], ww[:, FS_id_old_n - 2], xx[FS_id_old_n - 1], ww[:,FS_id_old_n - 1], x_si, xs, vs, self.eos, SIV_ORDER, fluid_normal, self.Interpolation_check)

            ww[:,FS_id_old_n] = Interpolation(ww[:,FS_id_old_n - 1],xx[FS_id_old_n - 1], ww_s, x_si, xx[FS_id_old_n], self.eos,  self.Interpolation_check)

        elif alg == 2:
            #Using extropolation between inside nodes
            xx = self.xx

            ww= self.ww_old_n
            ww[:,FS_id_old_n] = Interpolation(ww[:, FS_id_old_n - 2],xx[FS_id_old_n - 2],  ww[:,FS_id_old_n - 1], xx[FS_id_old_n - 1], xx[FS_id_old_n], self.eos,  self.Interpolation_check)

            ww= self.ww_n
            ww[:,FS_id_old_n] = Interpolation(ww[:, FS_id_old_n - 2], xx[FS_id_old_n - 2],  ww[:,FS_id_old_n - 1], xx[FS_id_old_n - 1], xx[FS_id_old_n], self.eos,  self.Interpolation_check)




class Structure:
    def __init__(self,ms, ks, N, L, thickness):
        self.ms = ms
        self.ks = ks
        self.qq_n = np.array([L/2.0, 0.0, 0.0])
        self.qq_old_n = np.array([L/2.0, 0.0, 0.0])
        self.qq_h = np.array([L/2.0, 0.0, 0.0])
        self.n = N
        self.L = L
        self.dx = float(L)/float(N)
        self.thickness = thickness
        self.t = 0
        self.mode = 3



    ########
    # return the fluid structure surrogate interface label
    # the first number is the surrogate boundary label of xs_{n-1}
    # the second number is the surrogate boundary label of xs_{n}
    ###############
    def FS_id_cal(self):

        xs_old = self.qq_old_n[0]

        xs = self.qq_n[0]

        return round((xs_old + self.thickness)/self.dx), round((xs + self.thickness)/self.dx)




    def Move_forward(self, force, dt,problem_type):

        forced_or_aero = problem_type[2]

        if(forced_or_aero == 'aero'):

            self.t = self.t + dt

            self.qq_old_n = self.qq_n

            [xs_0,vs_0,as_0] = self.qq_n

            as_0 = force/self.ms

            # there just predict the qq_{1/2}
            xs = xs_0 + dt*vs_0 + dt*dt*as_0/2.0

            vs = vs_0 + dt * as_0

            self.qq_n = np.array([xs,vs, as_0])

        elif(forced_or_aero == 'forced'):

            self.t = self.t + dt

            self.qq_old_n = self.qq_n

            self.qq_n = self.Forced_Motion(self.t)








    def Move_backward(self, fluid, dt):

        self.qq_n  = (self.qq_n + self.qq_old_n)/2.0

        self.t = self.t - dt
        '''

        [xs_0,vs_0,as_0] = self.qq_n

        as_0 = force/self.ms


        # there just predict the qq_{1/2}
        xs = xs_0 + dt*vs_0/2 + dt*dt*as_0/8.0

        vs = vs_0 + dt/2 * as_0

        self.qq_n = np.array([xs,vs, 0])
        '''

    def Forced_Motion(self,t):
        #self.t = t
        if self.mode == 1:
            xs = self.L/2 + 1.0/8.0 * t**4
            vs = 1.0/2.0 * t**3
            as_0 = 3.0/2.0*t**2
        elif self.mode == 2:
            xs = self.L/2.0 - 0.25*t
            vs = -0.25
            as_0 = 0

        elif self.mode == 3:
            u0 = 0.1;
            w  = 2.0;
            xs =  self.L/2.0 + u0*(1 - np.cos(np.pi*w*t))
            vs =  u0*np.pi*w*np.sin(np.pi*w*t)
            as_0 = -u0*np.pi*w*np.pi*w*np.sin(np.pi*w*t)
        #self.qq_n = np.array([xs,vs, as_0])
        return np.array([xs,vs, as_0])
