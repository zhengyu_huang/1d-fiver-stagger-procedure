__author__ = 'daniel'
#################################
#The body motion parameters can be set in body_motion.py
#Geometry, error check parameters can be set in global_variable.py
#Test, fluid parameter could be set here
#The initial condition could be set in FIVER.py, we have constant and linear initialization
######################################

from Error_check import *
Time = 0.5
#Time = 0.5
N = 200
L = 2.0
ms = 1.0
ks = 1.0
thickness = 1.0e-12
CFL = 0.05
ite = 5

#TODO we assume that Time divides by dt


#arg[0] is gamma
#arg[1:4] is desity, velocity and pressure
#in FIVER, there is an initalization function
#to initialize the fluid state,
#fluid_info = [gamma, density, velocity, pressure]
fluid_info = np.array([1.4, 1.225, 0. , 1.0])

#End time is arg[0] Time, arg[1] is the parameter about
#CFL number, if arg[1] == 0, CFL is const, dt =CFL*dx
time_info = np.array([Time, 0])


structure_info = [ms, ks, thickness]

#arg[0]: tube length
#arg[1]: number of cells
#CFL number, actually it is the ratio of dt/dx
mesh = [L,N,CFL]



#arg[0] is cell number N
#arg[1] is iteration time, in Error,
#we compute N, 2N ...
test_info = [N,ite]


#arg[0]: method A6 or A7
#arg[1]: implicit or explicit
#arg[2]: forced or aero
problem_type = ['A6','explicit','forced']
Result(fluid_info,structure_info, time_info, problem_type, [L,N,CFL])
Conservation_error(fluid_info,structure_info, time_info,test_info, problem_type, mesh)


problem_type = ['A6','explicit','aero']
Result(fluid_info,structure_info, time_info, problem_type, [L,N,CFL])
Conservation_error(fluid_info,structure_info, time_info,test_info, problem_type, mesh)



